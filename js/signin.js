function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
        const auth = firebase.auth();
        const email = txtEmail.value;
        const password = txtPassword.value;

        auth.signInWithEmailAndPassword(email, password)
            .then( e=>{
                create_alert('success','Log in');
                location.href="index.html";
            })
            .catch(()=> {
                create_alert('error','Log in again');
                txtEmail.value='';
                txtPassword.value='';
            });

    });

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        const auth = firebase.auth();
        var provider = new firebase.auth.GoogleAuthProvider(); 
        firebase.auth().signInWithPopup(provider)
            .then( function(result) {
                // 可以獲得 Google 提供 token，token可透過 Google API 獲得其他數據。  
                var token = result.credential.accessToken;
                var user = result.user;
                location.href="index.html";
              })
            .catch(()=> {
                create_alert('error','Log in again');
            });
    });

    btnSignUp.addEventListener('click', function () {        
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        const auth = firebase.auth();
        const email = txtEmail.value;
        const password = txtPassword.value;

        auth.createUserWithEmailAndPassword(email, password)
            .then(()=>{
                create_alert('success','Sign up');
                txtEmail.value='';
                txtPassword.value='';
            })
            .catch(() => {
                create_alert('error','Sign up again');
                txtEmail.value='';
                txtPassword.value='';
            });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};